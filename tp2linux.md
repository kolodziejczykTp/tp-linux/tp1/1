# TP2 pt. 1 : Gestion de service


# Sommaire

- [TP2 pt. 1 : Gestion de service](#tp2-pt-1--gestion-de-service)
- [Sommaire](#sommaire)
- [I. Un premier serveur web](#i-un-premier-serveur-web)
  - [1. Installation](#1-installation)
  - [2. Avancer vers la maîtrise du service](#2-avancer-vers-la-maîtrise-du-service)
- [II. Une stack web plus avancée](#ii-une-stack-web-plus-avancée)
  - [1. Intro](#1-intro)
  - [2. Setup](#2-setup)
    - [A. Serveur Web et NextCloud](#a-serveur-web-et-nextcloud)
    - [B. Base de données](#b-base-de-données)
    - [C. Finaliser l'installation de NextCloud](#c-finaliser-linstallation-de-nextcloud)

# I. Un premier serveur web

## 1. Installation

🖥️ **VM web.tp2.linux**

| Machine         | IP            | Service                 | Port ouvert | IP autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | 80          |   tous            |

> Ce tableau devra figurer à la fin du rendu, avec les ? remplacés par la bonne valeur (un seul tableau à la fin). Je vous le remets à chaque fois, à des fins de clarté, pour lister les machines qu'on a à chaque instant du TP.

🌞 **Installer le serveur Apache**

- paquet `httpd`

j'installe httpd
```
[graig@web ~]$ sudo dnf install httpd
```

j'installe vim
```
[graig@web ~]$ sudo dnf install vim
```

- la conf se trouve dans `/etc/httpd/`
  - le fichier de conf principal est `/etc/httpd/conf/httpd.conf`
  - je vous conseille **vivement** de virer tous les commentaire du fichier, à défaut de les lire, vous y verrez plus clair
    - avec `vim` vous pouvez tout virer avec `:g/^ *#.*/d`

je rentre dans le dossier conf
```
[graig@web ~]$ sudo vim /etc/httpd/conf/httpd.conf
```
j'enleve les commentaires avec la cmd 
```
:g/^ *#.*/d
```

> Ce que j'entends au-dessus par "fichier de conf principal" c'est que c'est **LE SEUL** fichier de conf lu par Apache quand il démarre. C'est souvent comme ça : un service ne lit qu'un unique fichier de conf pour démarrer. Cherchez pas, on va toujours au plus simple. Un seul fichier, c'est simple.  
**En revanche** ce serait le bordel si on mettait toute la conf dans un seul fichier pour pas mal de services.  
Donc, le principe, c'est que ce "fichier de conf principal" définit généralement deux choses. D'une part la conf globale. D'autre part, il inclut d'autres fichiers de confs plus spécifiques.  
On a le meilleur des deux mondes : simplicité (un seul fichier lu au démarrage) et la propreté (éclater la conf dans plusieurs fichiers).

🌞 **Démarrer le service Apache**

- le service s'appelle `httpd` (raccourci pour `httpd.service` en réalité)
  - démarrez le
  - faites en sorte qu'Apache démarre automatique au démarrage de la machine
  - ouvrez le port firewall nécessaire
```
LISTEN          0               128                                  *:80                                *:*             users:(("httpd",pid=24110,fd=4),("httpd",pid=24109,fd=4),("httpd",pid=24108,fd=4),("httpd",pid=24106,fd=4)) 
```
- utiliser une commande `ss` pour savoir sur quel port tourne actuellement Apache
```
sudo ss -alnpt
```

- [une petite portion du mémo est consacrée à `ss`](https://gitlab.com/it4lik/b2-linux-2021/-/blob/main/cours/memo/commandes.md#r%C3%A9seau)

regarder si le httpd est actif (deja automatiser via la cmd = sudo systemctl enable --now httpd)

```
[graig@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor prese>
   Active: active (running) since Wed 2021-10-06 10:11:45 CEST; 11s ago
     Docs: man:httpd.service(8)
 Main PID: 24106 (httpd)
   Status: "Running, listening on: port 80"
    Tasks: 213 (limit: 4934)
   Memory: 24.8M
   CGroup: /system.slice/httpd.service
           ├─24106 /usr/sbin/httpd -DFOREGROUND
           ├─24107 /usr/sbin/httpd -DFOREGROUND
           ├─24108 /usr/sbin/httpd -DFOREGROUND
           ├─24109 /usr/sbin/httpd -DFOREGROUND
           └─24110 /usr/sbin/httpd -DFOREGROUND

oct. 06 10:11:45 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
oct. 06 10:11:45 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
oct. 06 10:11:45 web.tp2.linux httpd[24106]: Server configured, listening on: p>
```


**En cas de problème** (IN CASE OF FIIIIRE) vous pouvez check les logs d'Apache :

```bash
# Demander à systemd les logs relatifs au service httpd
$ sudo journalctl -xe -u httpd

# Consulter le fichier de logs d'erreur d'Apache
$ sudo cat /var/log/httpd/error_log

# Il existe aussi un fichier de log qui enregistre toutes les requêtes effectuées sur votre serveur
$ sudo cat /var/log/httpd/access_log
```

🌞 **TEST**

- vérifier que le service est démarré
```
[graig@web ~]$ sudo systemctl status httpd
[sudo] Mot de passe de graig : 
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor prese>
   Active: active (running) since Wed 2021-10-06 10:11:45 CEST; 11min ago
     Docs: man:httpd.service(8)
 Main PID: 24106 (httpd)
   Status: "Total requests: 1; Idle/Busy workers 100/0;Requests/sec: 0.00149; B>
    Tasks: 213 (limit: 4934)
   Memory: 25.0M
   CGroup: /system.slice/httpd.service
           ├─24106 /usr/sbin/httpd -DFOREGROUND
           ├─24107 /usr/sbin/httpd -DFOREGROUND
           ├─24108 /usr/sbin/httpd -DFOREGROUND
           ├─24109 /usr/sbin/httpd -DFOREGROUND
           └─24110 /usr/sbin/httpd -DFOREGROUND

oct. 06 10:11:45 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
oct. 06 10:11:45 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
oct. 06 10:11:45 web.tp2.linux httpd[24106]: Server configured, listening on: p>

```
- vérifier qu'il est configuré pour démarrer automatiquement
- vérifier avec une commande `curl localhost` que vous joignez votre serveur web localement
```
[graig@web ~]$ curl localhost
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
```
- vérifier avec votre navigateur (sur votre PC) que vous accéder à votre serveur web
```
LISTEN          0               128                                  *:80                                *:*             users:(("httpd",pid=24110,fd=4),("httpd",pid=24109,fd=4),("httpd",pid=24108,fd=4),("httpd",pid=24106,fd=4)) 
```

## 2. Avancer vers la maîtrise du service

🌞 **Le service Apache...**

- donnez la commande qui permet d'activer le démarrage automatique d'Apache quand la machine s'allume

```
sudo systemctl enable --now httpd
```

- prouvez avec une commande qu'actuellement, le service est paramétré pour démarrer quand la machine s'allume

```
[graig@web ~]$ sudo systemctl is-enabled httpd
enabled
```

- affichez le contenu du fichier `httpd.service` qui contient la définition du service Apache

```
[graig@web ~]$ sudo systemctl cat httpd.service
# /usr/lib/systemd/system/httpd.service
# See httpd.service(8) for more information on using the httpd service.

# Modifying this file in-place is not recommended, because changes
# will be overwritten during package upgrades.  To customize the
# behaviour, run "systemctl edit httpd" to create an override unit.

# For example, to pass additional options (such as -D definitions) to
# the httpd binary at startup, create an override unit (as is done by
# systemctl edit) and enter the following:

#       [Service]
#       Environment=OPTIONS=-DMY_DEFINE

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

🌞 **Déterminer sous quel utilisateur tourne le processus Apache**

- mettez en évidence la ligne dans le fichier de conf principal d'Apache (`httpd.conf`) qui définit quel user est utilisé

```
[graig@web ~]$ sudo vim /etc/httpd/conf/httpd.conf
User apache
```

- utilisez la commande `ps -ef` pour visualiser les processus en cours d'exécution et confirmer que apache tourne bien sous l'utilisateur mentionné dans le fichier de conf

```
[graig@web ~]$ ps -ef | grep apache
apache     24107   24106  0 10:11 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     24108   24106  0 10:11 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
apache     24109   24106  0 10:11 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
apache     24110   24106  0 10:11 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
graig      24490    1468  0 11:42 pts/0    00:00:00 grep --color=auto apache
```

- vérifiez avec un `ls -al` le dossier du site (dans `/var/www/...`) 
  - vérifiez que tout son contenu est accesible à l'utilisateur mentionné dans le fichier de conf

```
[graig@web ~]$ sudo ls -al /usr/share/testpage/
total 12
drwxr-xr-x.  2 root root   24  6 oct.  10:04 .
drwxr-xr-x. 89 root root 4096  6 oct.  10:05 ..
-rw-r--r--.  1 root root 7621 11 juin  17:23 index.html
```

🌞 **Changer l'utilisateur utilisé par Apache**

- créez le nouvel utilisateur
  - pour les options de création, inspirez-vous de l'utilisateur Apache existant
    - le fichier `/etc/passwd` contient les informations relatives aux utilisateurs existants sur la machine
    - servez-vous en pour voir la config actuelle de l'utilisateur Apache par défaut
```
[graig@web ~]$ sudo vim /etc/passwd

graig:x:1000:1000:graig:/home/graig:/bin/bash
apache:x:48:48:Apache:/usr/share/httpd:/sbin/nologin
graig2:x:48:48:graig2:/usr/share/httpd:/sbin/nologin
```
- modifiez la configuration d'Apache pour qu'il utilise ce nouvel utilisateur
```
[graig@web home]$ sudo vim /httpd/conf/httpd.conf
User graig2
```
- redémarrez Apache
```
[graig@web home]$ sudo systemctl restart httpd
```
- utilisez une commande `ps` pour vérifier que le changement a pris effet

```
graig2     24848   24847  0 12:31 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
graig2     24849   24847  0 12:31 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
graig2     24850   24847  0 12:31 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
graig2     24851   24847  0 12:31 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
graig      25094    1468  0 12:43 pts/0    00:00:00 grep --color=auto graig2
```

🌞 **Faites en sorte que Apache tourne sur un autre port**

- modifiez la configuration d'Apache pour lui demande d'écouter sur un autre port de votre choix.

```
[graig@web home]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Active: active (running) since Wed 2021-10-06 17:07:03 CEST; 13s ago
   Status: "Running, listening on: port 80"
oct. 06 17:07:03 web.tp2.linux httpd[25516]: Server configured, listening on: port 80

[graig@web home]$ sudo vim /etc/httpd/conf/httpd.conf
#Je modifie la ligne listen 80 par Listen ...
```
-> on détermine maitenant qu'il est configuré pour écouter sur le port 80 

- ouvrez un nouveau port firewall, et fermez l'ancien.

```
[graig@web home]$ sudo firewall-cmd --add-port=8888/tcp --permanente
success
```

- redémarrez Apache.

```
[graig@web home]$ sudo systemctl restart httpd
[graig@web ~]$ sudo systemctl status httpd.service
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-10-06 18:30:11 CEST; 1min 21s ago
     Docs: man:httpd.service(8)
 Main PID: 838 (httpd)
   Status: "Running, listening on: port 8888"
    Tasks: 213 (limit: 4934)
   Memory: 39.2M
   CGroup: /system.slice/httpd.service
           ├─838 /usr/sbin/httpd -DFOREGROUND
           ├─862 /usr/sbin/httpd -DFOREGROUND
           ├─863 /usr/sbin/httpd -DFOREGROUND
           ├─864 /usr/sbin/httpd -DFOREGROUND
           └─866 /usr/sbin/httpd -DFOREGROUND

oct. 06 18:30:10 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
oct. 06 18:30:11 web.tp2.linux httpd[838]: AH00558: httpd: Could not reliably determine the server's fully qualified domain name, u>
oct. 06 18:30:11 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
oct. 06 18:30:11 web.tp2.linux httpd[838]: Server configured, listening on: port 8888
```

- prouvez avec une commande `ss` que Apache tourne bien sur le nouveau port choisi.

```
[graig@web home]$ ss -alntp
reponse : [graig@web ~]$ ss -alntp
State          Recv-Q         Send-Q                 Local Address:Port                   Peer Address:Port         Process         
LISTEN         0              128                          0.0.0.0:22                          0.0.0.0:*                            
LISTEN         0              128                             [::]:22                             [::]:*                            
LISTEN         0              128                                *:8888                              *:*                            

```

- vérifiez avec `curl` en local que vous pouvez joindre Apache sur le nouveau port.

```
[graig@web home]$ curl localhost:8888
reponse :[graig@web ~]$ curl localhost:8888
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
```

- vérifiez avec votre navigateur que vous pouvez joindre le serveur sur le nouveau port.

```
http://10.102.1.11:8888
```
![](apache.png)

📁 **Fichier `/etc/httpd/conf/httpd.conf`** (que vous pouvez renommer si besoin, vu que c'est le même nom que le dernier fichier demandé)

```
ServerRoot "/etc/htppd"

Listen 8888

Include conf.modules.d/*.conf

User graig2
Group apache


ServerAdmin root@localhost

<Directory />
    AllowOverride none
    Require all denied
<Directory>

DocumentRoot "/var/www/html"

<Directory "/var/www">
    Options Indexes FollowSymlinks

    AllowOverride None

    Require all granted
<Directory>

<IfModule dir_module>
    DirectoryIndex index.html
<IfModule>

<Files ".ht*">
    Require all denied
<Files>

ErrorLog "logs/error_log"

LogLevel warn

<IfModule log_config_module>
    LogFormat "%h %1 %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
    LogFormat "%h %1 %u %t \"%r\" %>s %b" common

  <IfModule logio_module>
      LogFormat "%h %1 %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %I %O" combinedio
  <IfModule>

    CustomLog "logs/access_log" combined
<IfModule>

<IfModule alias_module>


    ScriptAlias /gci-bin/ "/var/www/cgi-bin/"

<IfModule>

<Directory "/var/www/cgi-bin">
    AllowOverride None
    Options None
    Require all granted
<Directory>

<IfModule mime_module>
    TypeConfig /etc/mime.types

    AddType application/x-compress .Z
    AddType application/x-gzip .gz .tgz



    AddType text/html .shtml
    AddOutputFilter INCLUDES .shtml
<IfModule>

AddDefaultCharset UTF-8

<IfModule mime_magic_module>
    MIMEMagicFile conf/magic
<IfModule>

EnableSendfile on

IncludeOptional conf.d/*.conf
```

# II. Une stack web plus avancée

## 2. Setup

🖥️ **VM db.tp2.linux**

| Machine         | IP            | Service                 | Port ouvert | IP autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | 80          | tous             |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données | 3306        | 10.102.1.11            |

### A. Serveur Web et NextCloud

checklist good ! for db.tp2.linux

🌞 Install du serveur Web et de NextCloud sur `web.tp2.linux`

- je veux dans le rendu **toutes** les commandes réalisées

# A

## pour Nextcloud sur web.tp2.linux

voici toutes les cmd réalisés 

```
[graig@web ~]$ sudo dnf install epel-release
[graig@web ~]$ sudo dnf update
[graig@web ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm

[graig@web ~]$ sudo dnf module list php
Rocky Linux 8 - AppStream
Name                 Stream                  Profiles                                  Summary                              
php                  7.2 [d]                 common [d], devel, minimal                PHP scripting language               
php                  7.3                     common [d], devel, minimal                PHP scripting language               
php                  7.4                     common [d], devel, minimal                PHP scripting language               

Remi's Modular repository for Enterprise Linux 8 - x86_64
Name                 Stream                  Profiles                                  Summary                              
php                  remi-7.2                common [d], devel, minimal                PHP scripting language               
php                  remi-7.3                common [d], devel, minimal                PHP scripting language               
php                  remi-7.4                common [d], devel, minimal                PHP scripting language               
php                  remi-8.0                common [d], devel, minimal                PHP scripting language               
php                  remi-8.1                common [d], devel, minimal                PHP scripting language               

[graig@web ~]$ sudo dnf module enable php:remi-7.4
[graig@web ~]$ sudo dnf module list php
[graig@web ~]$ sudo dnf install httpd mariadb-server vim wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp

[graig@web ~]$ sudo systemctl enable httpd
[graig@web ~]$ sudo mkdri -p /var/www/sub-domains/web.tp2.linux
[graig@web ~]$ sudo mkdri -p /etc/httpd/sites-available/
[graig@web ~]$ sudo vi /etc/httpd/sites-available/web.tp2.linux.nextcloud
[graig@web ~]$ sudo ln -s /etc/httpd/sites-available/web.tp2.linux.nextcloud /etc/httpd/sites-enabled/
[graig@web ~]$ sudo mkdir -p /var/www/sub-domains/web.tp2.linux.com/html
[graig@web ~]$ cd /usr/share/zoneinfo
[graig@web ~]$ sudo vi /etc/opt/remi/php74/php.ini
chercher la ligne " ;date.timezone = ''Europe/Paris'' "
[graig@web zoneinfo]$ ls -al /etc/localtime
lrwxrwxrwx. 1 root root 34  5 oct.  09:22 /etc/localtime -> ../usr/share/zoneinfo/Europe/Paris

nextcloud est configué
```

  - n'oubliez pas la commande `history` qui permet de voir toutes les commandes tapées précédemment



Une fois que vous avez la page d'accueil de NextCloud sous les yeux avec votre navigateur Web, **NE VOUS CONNECTEZ PAS** et continuez le TP

📁 **Fichier `/etc/httpd/conf/httpd.conf`**  

```
ServerRoot "/etc/httpd"

Listen 80

Include conf.modules.d/*.conf

User graig
Group apache


ServerAdmin root@localhost


<Directory />
    AllowOverride none
    Require all denied
</Directory>
```


📁 **Fichier `/etc/httpd/sites-available/web.tp2.linux`**

j'ai rien dedans ....

### B. Base de données

🌞 **Install de MariaDB sur `db.tp2.linux`**

- déroulez [la doc d'install de Rocky](https://docs.rockylinux.org/guides/database/database_mariadb-server/)
- manipulation 
- je veux dans le rendu **toutes** les commandes réalisées

## pour MariaDB sur db.tp2.linux

voici toutes les cmd réalisés 

```
[graig@db ~]$ sudo dnf install mariadb-server
Installation des packages 

Terminé !

[graig@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.

[graig@db ~]$ sudo systemctl start mariadb
[graig@db ~]$ mysql_secure_installation
j'ai mis un mdp et j'ai suivie le protocol mariadb 
```

- vous repérerez le port utilisé par MariaDB avec une commande `ss` exécutée sur `db.tp2.linux`

```
[graig@db ~]$ ss -alpnt
State         Recv-Q        Send-Q               Local Address:Port                Peer Address:Port        Process        
LISTEN        0             128                        0.0.0.0:22                       0.0.0.0:*                          
LISTEN        0             80                               *:3306                           *:*                          
LISTEN        0             128                           [::]:22                          [::]:*        
```


🌞 **Préparation de la base pour NextCloud**

- une fois en place, il va falloir préparer une base de données pour NextCloud :
  - connectez-vous à la base de données à l'aide de la commande `sudo mysql -u root`

🌞 **Exploration de la base de données**

- afin de tester le bon fonctionnement de la base de données, vous allez essayer de vous connecter, comme NextCloud le fera :
  - depuis la machine `web.tp2.linux` vers l'IP de `db.tp2.linux`
  - vous pouvez utiliser la commande `mysql` pour vous connecter à une base de données depuis la ligne de commande
    - par exemple `mysql -u <USER> -h <IP_DATABASE> -p 
    ```
     MariaDB [(none)]> mysql -u nextcloud -h 10.102.1.11 -p 
     ```
- utilisez les commandes SQL fournies ci-dessous pour explorer la base

```sql
SHOW DATABASES;
USE <DATABASE_NAME>;
SHOW TABLES;
```

- trouver une commande qui permet de lister tous les utilisateurs de la base de données :

```
MariaDB [(none)]> select user,host from mysql.user;
+-----------+-------------+
| user      | host        |
+-----------+-------------+
| nextcloud | 10.102.1.11 |
| root      | 127.0.0.1   |
| root      | ::1         |
| root      | localhost   |
+-----------+-------------+
4 rows in set (0.000 sec)
```

### C. Finaliser l'installation de NextCloud

🌞 sur votre PC

- modifiez votre fichier `hosts` (oui, celui de votre PC, de votre hôte)

```
sudo vim /etc/hosts

10.102.1.11   web.tp2.linux
```

  - pour pouvoir joindre l'IP de la VM en utilisant le nom `web.tp2.linux`

- avec un navigateur, visitez NextCloud à l'URL `http://web.tp2.linux`

  - c'est possible grâce à la modification de votre fichier `hosts`
- on va vous demander un utilisateur et un mot de passe pour créer un compte admin
  - ne saisissez rien pour le moment
- cliquez sur "Storage & Database" juste en dessous
  - choisissez "MySQL/MariaDB"
  - saisissez les informations pour que NextCloud puisse se connecter avec votre base
- saisissez l'identifiant et le mot de passe admin que vous voulez, et validez l'installation

``` 
ca marche fck yeah !!
```

🌞 **Exploration de la base de données**

- connectez vous en ligne de commande à la base de données après l'installation terminée
- déterminer combien de tables ont été crées par NextCloud lors de la finalisation de l'installation

```
user@MacBook-Pro-de-User ~ % curl 10.102.1.11
<!DOCTYPE html>
<html>
<head>
	<script> window.location.href="index.php"; </script>
	<meta http-equiv="refresh" content="0; URL=index.php">
</head>
</html>
```

  - ***bonus points*** si la réponse à cette question est automatiquement donnée par une requête SQL

