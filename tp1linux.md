# 0. preparation de la machine.

## Se connecter en ssh.
```
user@MacBook-Pro-de-User ~ % ssh graig@10.101.1.11
The authenticity of host '10.101.1.11 (10.101.1.11)' can't be established.
ECDSA key fingerprint is SHA256:goiZfFxniQdtOjeDK4WJgWUi1zGLZz7sWovPjf21yHY.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '10.101.1.11' (ECDSA) to the list of known hosts.
graig@10.101.1.11's password:
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Wed Sep 22 12:25:21 2021
[graig@patron ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:7a:84:c9 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 85476sec preferred_lft 85476sec
    inet6 fe80::a00:27ff:fe7a:84c9/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:8b:f3:9d brd ff:ff:ff:ff:ff:ff
    inet 10.101.1.11/24 brd 10.101.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe8b:f39d/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

```
user@MacBook-Pro-de-User ~ % ssh graig@10.101.1.12
The authenticity of host '10.101.1.12 (10.101.1.12)' can't be established.
ECDSA key fingerprint is SHA256:goiZfFxniQdtOjeDK4WJgWUi1zGLZz7sWovPjf21yHY.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '10.101.1.12' (ECDSA) to the list of known hosts.
graig@10.101.1.12's password:
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Wed Sep 22 12:33:29 2021
[graig@patron ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:7a:84:c9 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 85386sec preferred_lft 85386sec
    inet6 fe80::a00:27ff:fe7a:84c9/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:92:19:66 brd ff:ff:ff:ff:ff:ff
    inet 10.101.1.12/24 brd 10.101.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe92:1966/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

## ping 1.1.1.1 + 8.8.8.8 + les VM entres elles.

```
[graig@patron ~]$ ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=63 time=18.9 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=63 time=18.6 ms
^C
--- 1.1.1.1 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1003ms
rtt min/avg/max/mdev = 18.623/18.756/18.889/0.133 ms
[graig@patron ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=63 time=19.2 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=63 time=18.6 ms
^C
--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1003ms
rtt min/avg/max/mdev = 18.581/18.892/19.203/0.311 ms
[graig@patron ~]$ ping 10.101.1.12
PING 10.101.1.12 (10.101.1.12) 56(84) bytes of data.
64 bytes from 10.101.1.12: icmp_seq=1 ttl=64 time=0.610 ms
64 bytes from 10.101.1.12: icmp_seq=2 ttl=64 time=0.451 ms
^C
--- 10.101.1.12 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1046ms
rtt min/avg/max/mdev = 0.451/0.530/0.610/0.082 ms
```


```
[graig@patron ~]$ ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=63 time=21.9 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=63 time=17.9 ms
^C
--- 1.1.1.1 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1003ms
rtt min/avg/max/mdev = 17.893/19.920/21.948/2.032 ms
[graig@patron ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=63 time=19.0 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=63 time=20.8 ms
^C
--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 19.036/19.927/20.818/0.891 ms
[graig@patron ~]$ ping 10.101.1.11
PING 10.101.1.11 (10.101.1.11) 56(84) bytes of data.
64 bytes from 10.101.1.11: icmp_seq=1 ttl=64 time=1.35 ms
64 bytes from 10.101.1.11: icmp_seq=2 ttl=64 time=0.473 ms
^C
--- 10.101.1.11 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1003ms
rtt min/avg/max/mdev = 0.473/0.913/1.354/0.441 ms
```

## static + ip + netmask.

```
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=static
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
NAME=enp0s8
UUID=87acb0a9-a86a-4e2e-bd29-84332dc2c6e2
DEVICE=enp0s8
ONBOOT=yes
IPADDR=10.101.1.11
NETMASK=255.255.255.0
```

```
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=static
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
NAME=enp0s8
UUID=87acb0a9-a86a-4e2e-bd29-84332dc2c6e2
DEVICE=enp0s8
ONBOOT=yes
IPADDR=10.101.1.12
NETMASK=255.255.255.0
```

## hostname changer.

```
[graig@patron ~]$ hostname
patron
[graig@patron ~]$ sudo reboot
Connection to 10.101.1.11 closed by remote host.
Connection to 10.101.1.11 closed.
user@MacBook-Pro-de-User ~ % ssh graig@10.101.1.11
[graig@node1 ~]$ hostname
node1.tp1.b2
```

```
[graig@patron ~]$ hostname
patron
[graig@patron ~]$ sudo reboot
Connection to 10.101.1.12 closed by remote host.
Connection to 10.101.1.12 closed.
user@MacBook-Pro-de-User ~ % ssh graig@10.101.1.12
[graig@node2 ~]$ hostname
node2.tp1.b2
```

## dig ynov.

```
[graig@node1 ~]$ dig ynov.com | grep SERVER
;; SERVER: 1.1.1.1#53(1.1.1.1)
```

```
[graig@node2 ~]$ dig ynov.com | grep SERVER
;; SERVER: 1.1.1.1#53(1.1.1.1)
```

## fichier /etc/hosts.

```
[graig@node1 ~]$ sudo nano /etc/hosts
[graig@node1 ~]$ ping node2
PING node2 (10.101.1.12) 56(84) bytes of data.
64 bytes from node2 (10.101.1.12): icmp_seq=1 ttl=64 time=1.27 ms
64 bytes from node2 (10.101.1.12): icmp_seq=2 ttl=64 time=0.621 ms
64 bytes from node2 (10.101.1.12): icmp_seq=3 ttl=64 time=0.422 ms
64 bytes from node2 (10.101.1.12): icmp_seq=4 ttl=64 time=0.565 ms
^C
```

```
[graig@node2 ~]$ sudo nano /etc/hosts
[sudo] Mot de passe de graig :
[graig@node2 ~]$ ping node1
PING node1 (10.101.1.11) 56(84) bytes of data.
64 bytes from node1 (10.101.1.11): icmp_seq=1 ttl=64 time=0.716 ms
64 bytes from node1 (10.101.1.11): icmp_seq=2 ttl=64 time=0.499 ms
64 bytes from node1 (10.101.1.11): icmp_seq=3 ttl=64 time=0.893 ms
^C
```

## Firewall.

```
[graig@node1 ~]$ sudo firewall-cmd --list-all | egrep "services|ports" | sed -n "1,2p"
  services: cockpit dhcpv6-client ssh
  ports: 80/tcp
```

```
[graig@node2 ~]$ sudo firewall-cmd --list-all | egrep "services|ports" | sed -n "1,2p"
  services: cockpit dhcpv6-client ssh
  ports: 80/tcp
```

# 1. Utilisateurs.

## Création et configuration.


## je le fais que sur une seul.
```
[graig@node1 ~]$ sudo useradd node1admin1 --home /home/node1admin1/ --create-home --shell /bin/bash
Creating mailbox file: File exists
[graig@node1 ~]$ sudo /etc/passwd
node1admin1:x:1001:1001::/home/node1admin1/:/bin/bash
```

```
[graig@node1 ~]$ sudo visudo
%wheel  ALL=(ALL)       ALL
%admin  ALL=(ALL)       ALL

[graig@node1 ~]$ sudo usermod -aG admin node1admin1
[graig@node1 ~]$ sudo nano /etc/group
node1admin1:x:1001:
admin:x:1003:node1admin1
```

## 2. SSH

```
[graig@node1 ~]$ ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/home/graig/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/graig/.ssh/id_rsa.
Your public key has been saved in /home/graig/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:0W6p+HY8/K9KaSfl4zXh0cn77CslrRaNcRw1LL2wR0U graig@node1.tp1.b2
The key's randomart image is:
+---[RSA 3072]----+
|              ooE|
|         .   o =.|
|        . .   * o|
|         o . o.*.|
|        S + . Xo.|
|       . o + = *.|
|      . .o= + O. |
|       ..o=+ * o.|
|       ....+=ooo=|
+----[SHA256]-----+


[graig@node1 ~]$ sudo ssh-copy-id
Usage: /bin/ssh-copy-id [-h|-?|-f|-n] [-i [identity_file]] [-p port] [[-o <ssh -o options>] ...] [user@]hostname
	-f: force mode -- copy keys without trying to check if they are already installed
	-n: dry run    -- no keys are actually copied
	-h|-?: print this help
```


# II.Partitionnement

## 1. Preparation de la VM

```
[graig@node1 ~]$ lsblck
-bash: lsblck : commande introuvable
[graig@node1 ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda           8:0    0 11,1G  0 disk
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0 10,1G  0 part
  ├─rl-root 253:0    0    9G  0 lvm  /
  └─rl-swap 253:1    0  1,1G  0 lvm  [SWAP]
sdb           8:16   0    3G  0 disk
sdc           8:32   0    3G  0 disk
sr0          11:0    1 1024M  0 rom
sr1          11:1    1  1,9G  0 rom
```

```
[graig@node1 ~]$ sudo pvcreate /dev/sdb
  Physical volume "/dev/sdb" successfully created.
[graig@node1 ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda           8:0    0 11,1G  0 disk
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0 10,1G  0 part
  ├─rl-root 253:0    0    9G  0 lvm  /
  └─rl-swap 253:1    0  1,1G  0 lvm  [SWAP]
sdb           8:16   0    3G  0 disk
sdc           8:32   0    3G  0 disk
sr0          11:0    1 1024M  0 rom
sr1          11:1    1  1,9G  0 rom
```

```
[graig@node1 ~]$ sudo pvdisplay
  --- Physical volume ---
  PV Name               /dev/sda2
  VG Name               rl
  PV Size               <10,11 GiB / not usable 4,00 MiB
  Allocatable           yes (but full)
  PE Size               4,00 MiB
  Total PE              2587
  Free PE               0
  Allocated PE          2587
  PV UUID               c4NdOj-NjY1-1bfK-QOct-ibda-Ydny-TEAWSM

  "/dev/sdb" is a new physical volume of "3,00 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sdb
  VG Name
  PV Size               3,00 GiB
  Allocatable           NO
  PE Size               0
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               Dxu4dA-hWKG-76rr-FXiQ-yvId-FJHF-nKEEIn

  "/dev/sdc" is a new physical volume of "3,00 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sdc
  VG Name
  PV Size               3,00 GiB
  Allocatable           NO
  PE Size               0
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               AIe7Rb-8fBc-bZ6e-tkA6-uLgt-vxWj-IsIbSo
```

```
[graig@node1 ~]$ sudo lvcreate -L 1G data VGnode1 -n LVnode1
  Volume group "data" not found
  Cannot process volume group data
[graig@node1 ~]$ sudo lvcreate -L 1G VGnode1 -n LVnode1
[sudo] Mot de passe de graig :
  Logical volume "LVnode1" created.
[graig@node1 ~]$ sudo lvcreate -L 1G VGnode1 -n LVnode1
  Logical Volume "LVnode1" already exists in volume group "VGnode1"
[graig@node1 ~]$ sudo lvcreate -L 1G VGnode1 -n LVnode1-2
  Logical volume "LVnode1-2" created.
[graig@node1 ~]$ sudo lvcreate -L 1G VGnode1 -n LVnode1-3
  Logical volume "LVnode1-3" created.
```

## 2. Partitionnement

```
[graig@node1 ~]$ sudo lvcreate -L 1G VGnode1 -n LVnode1-4
  Logical volume "LVnode1-4" created.
[graig@node1 ~]$ lsblk
NAME                 MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda                    8:0    0 11,1G  0 disk
├─sda1                 8:1    0    1G  0 part /boot
└─sda2                 8:2    0 10,1G  0 part
  ├─rl-root          253:0    0    9G  0 lvm  /
  └─rl-swap          253:1    0  1,1G  0 lvm  [SWAP]
sdb                    8:16   0    3G  0 disk
├─VGnode1-LVnode1    253:2    0    1G  0 lvm
└─VGnode1-LVnode1--2 253:3    0    1G  0 lvm
sdc                    8:32   0    3G  0 disk
├─VGnode1-LVnode1--3 253:4    0    1G  0 lvm
└─VGnode1-LVnode1--4 253:5    0    1G  0 lvm
sr0                   11:0    1 1024M  0 rom
sr1                   11:1    1  1,9G  0 rom
```

```
[graig@node1 ~]$ sudo mkfs -t ext4 /dev/VGnode1/LVnode1
mke2fs 1.45.6 (20-Mar-2020)
En train de créer un système de fichiers avec 262144 4k blocs et 65536 i-noeuds.
UUID de système de fichiers=cd406f46-69ee-4a1c-a1cf-84a09a5bbd36
Superblocs de secours stockés sur les blocs :
	32768, 98304, 163840, 229376

Allocation des tables de groupe : complété
Écriture des tables d'i-noeuds : complété
Création du journal (8192 blocs) : complété
Écriture des superblocs et de l'information de comptabilité du système de
fichiers : complété
```

```
[graig@node1 ~]$ sudo mkfs -t ext4 /dev/VGnode1/LVnode1-2
mke2fs 1.45.6 (20-Mar-2020)
En train de créer un système de fichiers avec 262144 4k blocs et 65536 i-noeuds.
UUID de système de fichiers=ba0ea797-dbc7-49f6-bcca-7167dbe50a2d
Superblocs de secours stockés sur les blocs :
	32768, 98304, 163840, 229376

Allocation des tables de groupe : complété
Écriture des tables d'i-noeuds : complété
Création du journal (8192 blocs) : complété
Écriture des superblocs et de l'information de comptabilité du système de
fichiers : complété
```

```
[graig@node1 ~]$ sudo mkfs -t ext4 /dev/VGnode1/LVnode1-3
mke2fs 1.45.6 (20-Mar-2020)
En train de créer un système de fichiers avec 262144 4k blocs et 65536 i-noeuds.
UUID de système de fichiers=99ed3c67-be99-43fb-ba93-87eaad31a548
Superblocs de secours stockés sur les blocs :
	32768, 98304, 163840, 229376

Allocation des tables de groupe : complété
Écriture des tables d'i-noeuds : complété
Création du journal (8192 blocs) : complété
Écriture des superblocs et de l'information de comptabilité du système de
fichiers : complété
```

```
[graig@node1 ~]$ sudo /dev/VGnode1/LVnode1/VGnode1 ext4 defaults 0 0
```
```
[graig@node1 ~]$ sudo /dev/sdb/LGnode1/LVnode1 ext4 defaults 0 0
```
il doit y avoir un probleme dans les noms que j'ai attribué à mes dossiers ou fichiers mais j'espere que techniquement la commande c'est ça. 


# III.Gestion de services

## 1. Interaction avec un service existant

```
[graig@node1 ~]$ sudo systemctl list-units -t service
UNIT                            LOAD   ACTIVE SUB     DESCRIPTION
atd.service                     loaded active running Job spooling tools
auditd.service                  loaded active running Security Auditing Service
crond.service                   loaded active running Command Scheduler
dbus.service                    loaded active running D-Bus System Message Bus
dracut-shutdown.service         loaded active exited  Restore /run/initramfs on>
firewalld.service               loaded active running firewalld - dynamic firew>
getty@tty1.service              loaded active running Getty on tty1
import-state.service            loaded active exited  Import network configurat>
kdump.service                   loaded active exited  Crash recovery kernel arm>
kmod-static-nodes.service       loaded active exited  Create list of required s>
libstoragemgmt.service          loaded active running libstoragemgmt plug-in se>
lvm2-monitor.service            loaded active exited  Monitoring of LVM2 mirror>
lvm2-pvscan@8:16.service        loaded active exited  LVM event activation on d>
lvm2-pvscan@8:2.service         loaded active exited  LVM event activation on d>
lvm2-pvscan@8:32.service        loaded active exited  LVM event activation on d>
mcelog.service                  loaded active running Machine Check Exception L>
NetworkManager-wait-online.service loaded active exited  Network Manager Wait O>
NetworkManager.service          loaded active running Network Manager
nis-domainname.service          loaded active exited  Read and set NIS domainna>
polkit.service                  loaded active running Authorization Manager
smartd.service                  loaded active running Self Monitoring and Repor>
sshd.service                    loaded active running OpenSSH server daemon
sssd.service                    loaded active running System Security Services >
```

```
[graig@node1 ~]$ systemctl is-enabled firewalld
enabled

[graig@node1 ~]$ systemctl is-active firewalld
active
```

## 2.Creation de services

## A. Unité simple

```
[graig@node1 ~]$ cat /etc/systemd/system web.service
cat: /etc/systemd/system: Is a directory
web.service
```

```
[graig@node1 ~]$ sudo systemctl start web
[graig@node1 ~]$
```
je sais pas si le faite qu'il ne se passe rien est normal je ne pense pas ^^

## B. Modification de l'unité

```
[graig@node1 ~]$ sudo nano /etc/systemd/system/web.service
usage: sudo -l [-AknS] [-g group] [-h host] [-p prompt] [-U user] [-u user]
  GNU nano 2.9.8            /etc/systemd/system/web.service

[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target

User=graig
```
